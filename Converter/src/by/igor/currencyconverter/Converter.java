package by.igor.currencyconverter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import android.os.Bundle;
import android.app.Activity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class Converter extends Activity {

	private final static String FILENAME = "file.txt";
	
    private double cursUSDBy = 8662.49;
    private double cursUSDUa = 8.1260; 
	
	private EditText tb_kursByUSD;
	private EditText tb_kursUaUSD;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_converter);
		
		final EditText tb_ua = (EditText)findViewById(R.id.tb_ua);
		final EditText tb_usd = (EditText)findViewById(R.id.tb_usd);
		final EditText tb_by = (EditText)findViewById(R.id.tb_by);
		
		tb_kursByUSD = (EditText)findViewById(R.id.tb_kursByUSD);
		tb_kursUaUSD = (EditText)findViewById(R.id.tb_UaUSD);
		
		final Button btn_clear = (Button)findViewById(R.id.btn_clear);
		final Button btn_update = (Button)findViewById(R.id.btn_update);
		final Button btn_save = (Button)findViewById(R.id.btn_load);
		final Button btn_st = (Button)findViewById(R.id.btn_st);
		
		
		OnEditorActionListener editorTb_ua = new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				    
		        double value_ua = Double.parseDouble(v.getText().toString());
		        double value_usd = value_ua / cursUSDUa;
		        double value_by = cursUSDBy * value_usd;
		        
		        tb_usd.setText(Double.toString(value_usd));
		        tb_by.setText(Double.toString(value_by));
		        
				return false;
			}
		};
		
		OnEditorActionListener editorTb_usd = new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				    
				double value_usd = Double.parseDouble(v.getText().toString());
		        double value_ua = value_usd*cursUSDUa;
		        double value_by = cursUSDBy * value_usd;
		        
		        tb_ua.setText(Double.toString(value_ua));
		        tb_by.setText(Double.toString(value_by));
		        
				return false;
			}
		};
		
		OnEditorActionListener editorTb_by = new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				    
		        double value_by = Double.parseDouble(v.getText().toString());
		        
		        double value_usd = value_by / cursUSDBy;
		        double value_ua = cursUSDUa * value_usd;
		        
		        tb_usd.setText(Double.toString(value_usd));
		        tb_ua.setText(Double.toString(value_ua));
		        
				return false;
			}
		};
		
		OnClickListener btn_clearListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tb_by.setText("");
				tb_ua.setText("");
				tb_usd.setText("");
			}
		};
		
		tb_ua.setOnEditorActionListener(editorTb_ua);
		tb_usd.setOnEditorActionListener(editorTb_usd);
		tb_by.setOnEditorActionListener(editorTb_by);
		btn_clear.setOnClickListener(btn_clearListener);
		
		openFile(FILENAME);
		if(tb_kursByUSD.getText().length()>0&&tb_kursUaUSD.getText().length()>0)
		{
			cursUSDBy = Double.parseDouble(tb_kursByUSD.getText().toString());
			cursUSDUa = Double.parseDouble(tb_kursUaUSD.getText().toString());
			Toast.makeText(this, "Loaded: BY" +cursUSDBy+" Ua"+cursUSDUa, Toast.LENGTH_LONG).show();
		}
		
		btn_update.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				saveFile(FILENAME);
				
			}
		});
		
		btn_save.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				openFile(FILENAME);
			}
		});
		
		btn_st.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cursUSDBy = 8662.49;
				cursUSDUa = 8.1260; 
				saveFileDefault(FILENAME);
			
				//Toast.makeText(this, "Loaded: BY" +cursUSDBy+" Ua" + cursUSDUa, Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_converter, menu);
		return true;
	}
	
	private void openFile(String fileName)
	{
		try
		{
			InputStream inStream = openFileInput(FILENAME);
			
			if(inStream != null)
			{
				InputStreamReader sr = 
						new InputStreamReader(inStream);
				
				BufferedReader reader = new BufferedReader(sr);
				String str;
				StringBuffer buffer = new StringBuffer();
				
				while((str = reader.readLine())!=null)
					buffer.append(str + "\n");
				
				inStream.close();
				
				String[] ss = buffer.toString().split("[ ]+");
				tb_kursByUSD.setText(ss[0]);
				tb_kursUaUSD.setText(ss[1]);
			}
		}
		catch(Throwable t) {
		Toast.makeText(this, "Exception: " +t.toString(), Toast.LENGTH_SHORT).show();
		
		}
	}
	
	private void saveFile(String FileName)
	{
		try
		{
			//getApplicationContext().
			/*getApplicationContext().openFileInput(arg0)*/
			OutputStream outStream = openFileOutput(FILENAME, 0);
			OutputStreamWriter sw = new OutputStreamWriter(outStream);
			
			sw.write(tb_kursByUSD.getText().toString());
			sw.write(" ");
			sw.write(tb_kursUaUSD.getText().toString());
			sw.close();
		}
		catch(Throwable t) {
		Toast.makeText(this, "Exception: " +t.toString(), Toast.LENGTH_SHORT).show();
		}
	}
	
	private void saveFileDefault(String FileName)
	{
		try
		{
			
			//getApplicationContext().openFileInput(arg0)
			OutputStream outStream = openFileOutput(FILENAME, 0);
			OutputStreamWriter sw = new OutputStreamWriter(outStream);
			
			sw.write("8662.49");
			sw.write(" ");
			sw.write("8.1260");
			sw.close();
		}
		catch(Throwable t) {
		Toast.makeText(this, "Exception: " +t.toString(), Toast.LENGTH_SHORT).show();
		}
	}

}
